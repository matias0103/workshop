﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class SyntaxShowcase
    {

        string text = "Foreach loop example text";
        private void Syntax()
        {

            //If else statement
            if (text.Length > 5)
            {
                // Do Something
            }
            else
            {
                // Do Something else
            }

            //for loop
            for (int i = 0; i <= 10; i++)
            {
                // do something
            }

            //switch
            switch (Console.ReadLine())
            {
                case "case1":
                    //do something
                    break;
                case "case2":
                    //do something
                    break;
                default:
                    //do something
                    break;
            }

            //foreach
            foreach (char character in text)
            {
                if (character == 'e')
                {
                    // Do something
                }

            }

        }

        //Method example
        //public since we want to be able to access it in other classes and void since we aren't returning a specific 
        //datatype, like a string, but rather just executing some code.
        public void MethodExample(string text)
        {
            Console.WriteLine(text);
        }
    }
}
