﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //a few datatype examples 
            int intExample = 5;
            double doubleExample = 1.32;
            bool boolExample = false;
            char charExample = 'a';


            //How to create, write and read from a file
            //---------------------------------------------------
            string pathToFile = "MyTextFile1";
            string textFromFile = "";

            //File.Create creates a new file or overwrites the file if it already exists
            File.Create(pathToFile).Dispose();

            //This is how you write to a file.
            StreamWriter sw = new StreamWriter(pathToFile, true);
            sw.Write("This was just added to the file using StreamWriter");
            sw.Close();

            //This is how you read from a file
            StreamReader sr = new StreamReader(pathToFile);
            textFromFile = sr.ReadToEnd();
            Console.Write(textFromFile);
            sr.Close();

            //Console.ReadKey();
            //----------------------------------------------------


            //How to create an object and call a method on it
            //----------------------------------------------------
            //SyntaxShowcase ss =  SyntaxShowcase();
            //ss.MethodExample("Written to console using a method from another class");
            //----------------------------------------------------



            //Assignments
            FileHandler fh = new FileHandler();
            //fh.ReadFromFile(pathToFile);
            //fh.WriteToFile("Test");
            //fh.OverwriteFile();
            //fh.OverwriteFileWithSwitch();
            //fh.AddNumbersZeroToTenToFile();
            fh.FrequencyOfASpecificCharacterInFile('e');

            Console.ReadKey();
        }
    }
}
