﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ConsoleApp4
{
    class FileHandler
    {
        string pathToFile = "MyTextFile1";
        string textFromFile = "";

        public void WriteToFile(string textToAdd)
        {
            StreamWriter sw = new StreamWriter(pathToFile, true);
            sw.Write(" " + textToAdd);
            sw.Close();
        }

        public void ReadFromFile(string pathToFile)
        {
            StreamReader sr = new StreamReader(pathToFile);
            textFromFile = sr.ReadToEnd();
            Console.WriteLine(textFromFile);
            sr.Close();
        }

        public void OverwriteFile()
        {
            File.Create(pathToFile).Dispose();
        }

        public void OverwriteFileWithSwitch()
        {
            Console.WriteLine("Overwrite the file yes/no?");


            switch (Console.ReadLine().ToLower())
            {
                case "yes":
                    File.Create(pathToFile).Dispose();
                    Console.WriteLine("File has been overwritten");
                    break;
                case "no":
                    Console.WriteLine("File was not overwritten");
                    break;
                default:
                    Console.WriteLine("Your input didn't match any of the other cases");
                    break;
            }

        }

        public void AddNumbersZeroToTenToFile()
        {
            StreamWriter sw = new StreamWriter(pathToFile);

            for (int i = 0; i <= 10; i++)
            {
                sw.Write(i + " ");
            }
            sw.Close();

            ReadFromFile(pathToFile);

        }

        public void FrequencyOfASpecificCharacterInFile(char characterToCheckFor)
        {
            int count = 0;

            StreamReader sr = new StreamReader(pathToFile);
            textFromFile = sr.ReadToEnd();
            sr.Close();

            foreach (char character in textFromFile)
            {
                if (character == characterToCheckFor)
                    count++;
            }

            Console.WriteLine("\nThe character '{0}' appeared this many times: {1} ", characterToCheckFor, count);
        }
    }
}
